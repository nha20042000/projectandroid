package learnandroi.dba.projectmobi.main;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;

import learnandroi.dba.projectmobi.R;

public class MainActivity extends AppCompatActivity {


    private ViewPager2 mViewpager2;
    private BottomNavigationView bottomNavigationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewpager2 = findViewById(R.id.main_viewpager2);
        bottomNavigationView = findViewById(R.id.bottom_nav);
        MainViewpagerAdapter mMainViewpagerAdapter = new MainViewpagerAdapter(this);
        mViewpager2.setAdapter(mMainViewpagerAdapter);
        bottomNavigationView.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                if(id == R.id.action_home ){
                    mViewpager2.setCurrentItem(0);
                } else if(id == R.id.action_categrory){
                    mViewpager2.setCurrentItem(1);
                } else {
                    mViewpager2.setCurrentItem(2);
                }
                return true;
            }
        });
        mViewpager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                switch (position){
                    case 0:
                        bottomNavigationView.getMenu().findItem(R.id.action_home).setChecked(true);
                        break;
                    case 1:
                        bottomNavigationView.getMenu().findItem(R.id.action_categrory).setChecked(true);
                        break;
                    case 2:
                        bottomNavigationView.getMenu().findItem(R.id.action_user).setChecked(true);
                        break;
                }
            }
        });
    }
}