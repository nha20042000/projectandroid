package learnandroi.dba.projectmobi.main;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import learnandroi.dba.projectmobi.categrory.CategoryFrgm;
import learnandroi.dba.projectmobi.home.HomeFrgm;
import learnandroi.dba.projectmobi.user.UserFrgm;

public class MainViewpagerAdapter extends FragmentStateAdapter {


    public MainViewpagerAdapter(@NonNull FragmentActivity fragmentActivity) {
        super(fragmentActivity);
    }

//    public MainViewpagerAdapter() {
//        super();
//    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
            switch (position){
                case 0: return new HomeFrgm();
                case 1: return new CategoryFrgm();
                case 2: return new UserFrgm();
                default: return new HomeFrgm();
            }
    }

    @Override
    public int getItemCount() {
        return 3;
    }
}
