package learnandroi.dba.projectmobi.user;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnandroi.dba.projectmobi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFrgm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFrgm extends Fragment {
    private View mview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview =  inflater.inflate(R.layout.fragment_user_frgm, container, false);
        return mview;
    }
}