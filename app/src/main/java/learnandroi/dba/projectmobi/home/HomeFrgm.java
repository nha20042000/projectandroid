package learnandroi.dba.projectmobi.home;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnandroi.dba.projectmobi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFrgm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFrgm extends Fragment {
    private View mView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home_frgm, container, false);
        return mView;
    }
}