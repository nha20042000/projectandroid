package learnandroi.dba.projectmobi.categrory;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import learnandroi.dba.projectmobi.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoryFrgm#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CategoryFrgm extends Fragment {
    private View mView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment\
        mView = inflater.inflate(R.layout.fragment_category, container, false);
        return mView;
    }
}